# twitter-geo-app

Adventures in streaming pipelines with Kafka and Helm. This app consumes tweet data from the 
[Twitter Sample Stream API v2](https://developer.twitter.com/en/docs/twitter-api/tweets/volume-streams/api-reference/get-tweets-sample-stream)
and retrieves the most recent temperature in Fahrenheit from [WeatherAPI.com](https://www.weatherapi.com/docs/) 
for coordinates associated with tweets. While not intrinsically useful, this exercise was good practice in 
building a pipeline on top of a stream.

## Description
twitter-geo is a helm deployment and can be deployed to a kubernetes cluster. This was developed locally on minikube.

## Local Deployment and Installation via Helm
### Prerequisites
- [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- [Helm](https://helm.sh/docs/intro/quickstart/)
- [minikube](https://minikube.sigs.k8s.io/docs/start/)
- [Docker](https://docs.docker.com/get-docker/)
- [a Twitter developer Bearer Token](https://developer.twitter.com/en/docs/authentication/oauth-2-0/bearer-tokens)
- [WeatherAPI.com account and api key](https://www.weatherapi.com/signup.aspx)
### Minikube Set-up
```bash
# allocate generous resources for Kafka (ensure that Docker can accomodate)
minikube config set cpu 4 --profile custom
minikube config set memory 4096 --profile custom
# start minikube
minikube start --profile custom
eval $(minikube -p custom docker-env)
```
### Build the twitter-geo docker image
```bash
# from within a clone of this repository, build the image
docker build -t twitter-geo:latest .
# alternatively pull
docker pull kalebdfischer/twitter-geo
```
### Configure Helm
```bash
helm repo add bitnami https://charts.bitnami.com/bitnami
```
### Deploy
Install and deploy helm via the following:
```bash
helm upgrade --install --set config.twitterBearerToken=<YOUR TOKEN HERE>,config.weatherAPIKey=<YOUR API Key Here>,image.tag=latest,image.pullPolicy=Always geo-twitter ./helm/twitter-geo
```

## Configurable Values
The environment variable `SLIDING_AVERAGE_WINDOW_SIZE` is used to set the window size used by the 
[raw_to_sliding_average_temperature processor](twitter_geo_app/raw_to_sliding_average_temperature.py) to 
compute sliding/rolling temperature averages. This can be modified to an integer between 2 and 100 in [deployment.yaml](helm/twitter-geo/templates/deployment.yaml) under spec.template.spec.containers[2].env[1].name
or by removing the value entirely and setting a different default in [twitter_geo_app.settings.py](twitter_geo_app/settings.py)

Additional info about the bitnami chart can be found at the [Bitnami Kafka Chart on GitHub](https://github.com/bitnami/charts/tree/master/bitnami/kafka/#installing-the-chart)
## Outputs
- raw_temperatures_in_Fahrenheit.csv
- sliding_average_temperatures_in_Fahrenheit.csv
raw temperature and sliding temperature output files can be retrieved from a persistent volume mount at /data 
within the geo-twitter-twitter-geo pod as follows while the deployment is running in Minikube:
```bash
# get pod name
twitter_geo_pod=$(kubectl get pods | grep geo-twitter-twitter-geo | awk '{ print $1 }')
# copy files out of pods
kubectl cp ${twitter_geo_pod}:/data/raw_temperatures_in_Fahrenheit.csv -c twitter-geo-coordinate-to-temperature $(pwd)/data/raw_temperatures_in_Fahrenheit.csv
kubectl cp ${twitter_geo_pod}:/data/sliding_average_temperatures_in_Fahrenheit.csv -c twitter-geo-temp-sliding-average $(pwd)/data/sliding_average_temperatures_in_Fahrenheit.csv

```
Sample output files can be found under [data/](/data/) within this repository.

## Running Tests
### Poetry Setup

Initialize the Python environment via poetry:

```bash
poetry install
```

## Run the Tests

```bash
poetry run python -m pytest tests/
```
## Stop the Deployment
Run the following to uninstall Helm and stop minikube:
```bash
helm uninstall geo-twitter
minikube stop --profile custom
```

## Roadmap
Since it's fun to imagine, here's what an AWS-flavoured deployment might look like for streaming
and storing our Twitter and weather data:
![High-level AWS deployment diagram](images/dream_big_on_AWS.png "High-level AWS deployment diagram")


Conceptually similar, but with dedicated Amazon MSK, real secret management, database storage of records, 
and additional stream producers (provided you invest the dollars for more serious access to the Twitter and Weather APIs)
