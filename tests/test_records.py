from unittest import mock

from twitter_geo_app import records


def test_geo_record_dict_output_matches_input_kwargs(
    test_geo_record_dict,
):
    expected = test_geo_record_dict
    result = records.GeoRecord(**expected)
    assert result.dict() == expected


def test_raw_temperature_record_dict_matches_input_kwargs(
    test_raw_temperature_record_dict,
):
    expected = test_raw_temperature_record_dict
    result = records.RawTemperatureRecord(**expected)
    assert result.dict() == expected


def test_sliding_average_temperature_record_dict_matches_input_kwargs(
    test_sliding_average_temperature_record_dict,
):
    expected = test_sliding_average_temperature_record_dict
    result = records.AverageTemperatureRecord(**expected)
    assert result.dict() == expected


def test_sliding_average_temperature_record_dict_sets_unix_time_if_not_provided(
    test_sliding_average_temperature_record_dict,
):
    mock_return_value = 0
    with mock.patch(
        "twitter_geo_app.records.current_unix_time_utc", return_value=mock_return_value
    ) as mock_current_utc_epoch:
        expected = test_sliding_average_temperature_record_dict.copy()
        del expected["unix_time_utc"]
        result = records.AverageTemperatureRecord(**expected)
        assert mock_current_utc_epoch.called
        expected["unix_time_utc"] = mock_return_value
        assert result.dict() == expected
