from collections import deque

import numpy as np

from twitter_geo_app.raw_to_sliding_average_temperature import (
    SlidingAverager,
    get_sliding_average_temperature_record,
)
from twitter_geo_app.records import RawTemperatureRecord


def test_sliding_averager():
    window_size = 4
    sliding_averager = SlidingAverager(window_size)
    values = deque()
    for i in range(1, 10):
        values.append(i)
        current_average = sliding_averager.calculate(i)
        if i <= window_size:
            expected_values = values
            assert len(sliding_averager.buffer) <= window_size
        else:
            expected_values = deque(list(values)[-window_size:])
            assert len(sliding_averager.buffer) == window_size
        assert current_average == np.average(expected_values)
        assert np.average(expected_values) == current_average
        assert expected_values == sliding_averager.buffer


def test_get_sliding_average_temperature_record(test_raw_temperature_record_dict):
    window_size = 4
    raw_temp_record = RawTemperatureRecord(**test_raw_temperature_record_dict)
    sliding_averager = SlidingAverager(window_size)
    for i in range(1, 10):
        raw_temp_record.temperature_fahrenheit = i
        avg_temp_record = get_sliding_average_temperature_record(
            sliding_averager, raw_temp_record
        )
        assert avg_temp_record.temperature_f_sliding_average == np.average(
            sliding_averager.buffer
        )
        assert avg_temp_record.window_size == min(window_size, i)
