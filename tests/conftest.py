import pytest
from tweepy import Tweet, Place, StreamResponse
from tweepy.client import BaseClient


@pytest.fixture(scope="function")
def test_geo_record_dict():
    return {
        "tweet_id": "1136048014974423040",
        "latitude": 44.962969,
        "longitude": 93.167236,
        "tweet_created_unix_time_utc": 1559689928,
    }


@pytest.fixture(scope="function")
def test_raw_temperature_record_dict():
    return {
        "last_updated_unix_time": -238966200,
        "temperature_fahrenheit": 60.0,
        "request_unix_time": -238964400,
    }


@pytest.fixture(scope="function")
def test_sliding_average_temperature_record_dict():
    return {
        "temperature_f_sliding_average": 32.0,
        "window_size": 5,
        "unix_time_utc": -238964410,
    }


@pytest.fixture(scope="function")
def test_current_weather_dict():
    return {
        "current": {"temp_f": 60.0, "last_updated_epoch": -238966200},
        "location": {"localtime_epoch": -238964400},
    }


@pytest.fixture(scope="function")
def test_bbox_list():
    return [0.0088, 51.4924, 0.0108, 51.4944]


@pytest.fixture(scope="function")
def mock_tweet_response_json():
    return {
        "data": {
            "full_name": "bleh",
            "id": "1136048014974423040",
            "created_at": "2019-06-04T23:12:08.000Z",
            "geo": {"place_id": "01a9a39529b27f36"},
            "text": "garbage",
        },
        "includes": {
            "places": [
                {
                    "geo": {
                        "type": "Feature",
                        "bbox": [93.157236, 44.952969, 93.177236, 44.972969],
                        "properties": {},
                    },
                    "full_name": "City",
                    "id": "an_id",
                }
            ]
        },
    }


@pytest.fixture(scope="function")
def mock_stream_response(mock_tweet_response_json):
    tweet = Tweet(mock_tweet_response_json["data"])
    includes = BaseClient._process_includes(
        BaseClient, mock_tweet_response_json["includes"]
    )
    includes["places"] = [Place(place) for place in includes["places"]]
    return StreamResponse(tweet, includes, None, None)
