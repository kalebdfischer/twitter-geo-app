from unittest import mock

from twitter_geo_app.twitter_stream_producer import (
    get_bbox_centroid,
    response_to_geo_record,
)


def test_get_bbox_centroid(test_bbox_list):
    expected = (51.4934, 0.0098)
    result = get_bbox_centroid(test_bbox_list)
    assert result == expected


def test_response_to_geo_record_returns_geo_record(
    test_geo_record_dict, mock_stream_response
):

    result = response_to_geo_record(mock_stream_response)
    assert result.dict() == test_geo_record_dict
