from twitter_geo_app import coordinate_to_temperature_processor


def test_current_weather_to_raw_temperature_record_returns_raw_temperature_record(
    test_raw_temperature_record_dict, test_current_weather_dict
):
    result = (
        coordinate_to_temperature_processor.current_weather_to_raw_temperature_record(
            test_current_weather_dict
        )
    )
    assert (
        result.temperature_fahrenheit == test_current_weather_dict["current"]["temp_f"]
    )
    assert (
        result.last_updated_unix_time
        == test_current_weather_dict["current"]["last_updated_epoch"]
    )
    assert (
        result.request_unix_time
        == test_current_weather_dict["location"]["localtime_epoch"]
    )
    assert test_raw_temperature_record_dict == result.dict()
