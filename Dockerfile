FROM python:3.9-buster AS base

ENV POETRY_HOME=/opt/poetry
ENV POETRY_VERSION=1.1.4
ENV POETRY_VIRTUALENVS_CREATE=false
ENV STARTING_PATH = $PATH
ENV PATH="$POETRY_HOME/bin:$STARTING_PATH"
RUN curl -fLSs https://raw.githubusercontent.com/python-poetry/poetry/$POETRY_VERSION/get-poetry.py | python

VOLUME /data

# Set workdir, copy poetry.lock
WORKDIR /src/app
COPY pyproject.toml poetry.lock /src/app/
ENV PYTHONPATH=/src/app
RUN poetry install

COPY . /src/app
