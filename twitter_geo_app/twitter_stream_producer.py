import logging
import signal
import time
from typing import List, Tuple, Union

import numpy as np
from kafka import KafkaProducer
from tweepy import StreamingClient, StreamResponse, Tweet

from twitter_geo_app.records import GeoRecord
from twitter_geo_app.settings import TWITTER_BEARER_TOKEN, TWITTER_GEO_TOPIC, KAFKA_HOST

log = logging.getLogger(__name__)


def get_bbox_centroid(bbox: List[float]) -> Tuple[float, float]:
    """
    Compute the centroid from an array returned from a list in the
    retrieved from the includes.places.geo.bbox attribute of a twitter
    v2 stream response

    Args:
        bbox: list in format of [longitude_start, latitude_start, longitude_end,
            latitude_end] (per
                https://developer.twitter.com/en/docs/labs/
                tweets-and-users/api-reference/get-tweets
            )

    Returns:
        tuple: latitude, longitude
    """
    bbox_array = np.asarray(bbox)
    latitude = np.average(bbox_array[1::2])
    longitude = np.average(bbox_array[0::2])
    return latitude, longitude


def response_to_geo_record(response: StreamResponse) -> Union[None, GeoRecord]:
    tweet_id = "failed_to_get_id"
    try:
        places = response.includes.get("places")
        if places and response.data.created_at:
            tweet_id = response.data.id
            bbox = [place.geo for place in places if place.geo][0]["bbox"]
            latitude, longitude = get_bbox_centroid(bbox)

            created_at_unix_time = int(response.data.created_at.timestamp())
            geo_record = GeoRecord(
                tweet_id=tweet_id,
                latitude=latitude,
                longitude=longitude,
                tweet_created_unix_time_utc=created_at_unix_time,
            )
            return geo_record
    except:
        log.error(
            "Error parsing stream response for tweet %s - skipping...",
            tweet_id,
            exc_info=True,
        )


class GeoStreamer(StreamingClient):
    def __init__(self, **kwargs):
        super(GeoStreamer, self).__init__(TWITTER_BEARER_TOKEN, **kwargs)
        print(f"connecting to Kafka at {KAFKA_HOST}")
        self.producer = KafkaProducer(
            api_version=(0, 10, 1), bootstrap_servers=KAFKA_HOST
        )

    def on_response(self, response: StreamResponse):
        record = response_to_geo_record(response)

        if record:
            print(f"emitting to record topic {TWITTER_GEO_TOPIC}: {record}")
            self.producer.send(TWITTER_GEO_TOPIC, value=record.serialize())

    def stream(self):
        self.sample(
            tweet_fields=["geo", "created_at"],
            expansions="geo.place_id",
            place_fields="geo",
        )


if __name__ == "__main__":
    print("twitter_stream_producer.py starting...")
    geo_streamer = GeoStreamer(wait_on_rate_limit=True)

    def on_sigterm(signum, frame):
        """Handle sigterm"""
        geo_streamer.disconnect()
        log.warning("Exiting...")
        time.sleep(1)
        raise SystemExit()

    signal.signal(signal.SIGTERM, on_sigterm)

    print("starting twitter streaming...")
    geo_streamer.stream()
