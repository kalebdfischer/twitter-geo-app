import csv
import os
from collections import deque
from typing import Union

import numpy as np
from kafka import KafkaConsumer, KafkaProducer

from twitter_geo_app.records import (
    deserialize_record,
    RawTemperatureRecord,
    AverageTemperatureRecord,
)
from twitter_geo_app.settings import (
    RAW_TEMPERATURE_TOPIC,
    KAFKA_HOST,
    SLIDING_AVERAGE_WINDOW_SIZE,
    SLIDING_AVERAGE_TOPIC,
    SLIDING_AVERAGE_FILE_PATH,
)


class SlidingAverager:
    def __init__(self, window_size: int):
        self.buffer = deque()
        self.window_size = window_size

    def calculate(self, val: Union[float, int]) -> float:
        if len(self.buffer) == self.window_size:
            self.buffer.popleft()
            self.buffer.append(val)
        else:
            self.buffer.append(val)
        sliding_avg = np.average(self.buffer)
        print(f"Sliding average: {sliding_avg}")
        return sliding_avg


def get_sliding_average_temperature_record(
    sliding_averager: SlidingAverager, raw_temp_record: RawTemperatureRecord
) -> AverageTemperatureRecord:
    average_temp = sliding_averager.calculate(raw_temp_record.temperature_fahrenheit)
    window_size = len(sliding_averager.buffer)
    return AverageTemperatureRecord(
        temperature_f_sliding_average=average_temp, window_size=window_size
    )


def process_raw_temperature():
    consumer = KafkaConsumer(
        RAW_TEMPERATURE_TOPIC, api_version=(0, 10, 1), bootstrap_servers=KAFKA_HOST
    )
    producer = KafkaProducer(api_version=(0, 10, 1), bootstrap_servers=KAFKA_HOST)
    print(
        f"starting sliding temperature processing from kafka topic {RAW_TEMPERATURE_TOPIC}..."
    )
    write_path = SLIDING_AVERAGE_FILE_PATH
    write_header = not os.path.isfile(write_path)
    sliding_averager = SlidingAverager(SLIDING_AVERAGE_WINDOW_SIZE)
    headers = list(AverageTemperatureRecord.schema()["properties"].keys())
    with open(write_path, "a") as out_file:
        csv_writer = csv.DictWriter(out_file, fieldnames=headers)
        for message in consumer:
            print(f"received {message.value}")
            raw_temp_record = RawTemperatureRecord(**deserialize_record(message.value))
            avg_temp_record = get_sliding_average_temperature_record(
                sliding_averager, raw_temp_record
            )
            if write_header:
                csv_writer.writeheader()
                write_header = False
            print("writing temperature data")
            csv_writer.writerow(avg_temp_record.dict())
            out_file.flush()
            producer.send(SLIDING_AVERAGE_TOPIC, avg_temp_record.serialize())


if __name__ == "__main__":
    print("coordinate_to_temperature_processor.py starting...")
    process_raw_temperature()
