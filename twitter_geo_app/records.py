import json
import time
from typing import Dict, Optional

from pydantic import BaseModel, confloat, conint, root_validator


def current_unix_time_utc() -> int:
    return int(time.time())


def deserialize_record(serialized_record: bytes) -> dict:
    return json.loads(serialized_record.decode("utf-8"))


class TopicRecord(BaseModel):
    def serialize(self):
        return json.dumps(self.dict()).encode("utf-8")


class GeoRecord(TopicRecord):
    tweet_id: str
    latitude: confloat(ge=-90.0, le=90)
    longitude: confloat(ge=-180.0, le=180)
    tweet_created_unix_time_utc: int


class RawTemperatureRecord(TopicRecord):
    last_updated_unix_time: int
    temperature_fahrenheit: confloat(ge=-459.67)
    request_unix_time: int


class AverageTemperatureRecord(TopicRecord):
    temperature_f_sliding_average: confloat(ge=-459.67)
    window_size: conint(ge=1, le=100)
    unix_time_utc: Optional[int] = None

    @root_validator
    def set_unix_time_utc(cls, values) -> Dict:
        if values["unix_time_utc"] is None:
            values["unix_time_utc"] = current_unix_time_utc()
        return values
