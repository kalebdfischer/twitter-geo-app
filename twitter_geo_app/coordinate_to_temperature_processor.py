import csv
import logging
import os
from typing import Union

import requests
from kafka import KafkaConsumer, KafkaProducer

from twitter_geo_app.records import GeoRecord, RawTemperatureRecord, deserialize_record
from twitter_geo_app.settings import (
    CURRENT_WEATHER_REQUEST_URL,
    WEATHER_API_KEY,
    KAFKA_HOST,
    TWITTER_GEO_TOPIC,
    RAW_TEMPERATURE_TOPIC,
    RAW_TEMPERATURE_FILE_PATH,
)

log = logging.getLogger(__name__)


def get_current_weather_json(latitude: float, longitude: float) -> dict:
    """
    Submit a get request to weatherapi current endpoint and return the response
    json dict

    Args:
        latitude: latitude for which to retrieve current whether
        longitude: longitude for which to retrieve current whether

    Returns:
        dictionary representing the json response from the weatherapi current
        endpoint
    """
    # http://www.weatherapi.com/docs/#intro-request for more information
    param_dict = {
        "q": f"{latitude},{longitude}",
        "key": WEATHER_API_KEY,
    }
    response = requests.get(CURRENT_WEATHER_REQUEST_URL, params=param_dict)
    response.raise_for_status()
    return response.json()


def current_weather_to_raw_temperature_record(
    current_weather_dict: dict,
) -> RawTemperatureRecord:
    record_dict = {
        "last_updated_unix_time": current_weather_dict["current"]["last_updated_epoch"],
        "temperature_fahrenheit": current_weather_dict["current"]["temp_f"],
        "request_unix_time": current_weather_dict["location"]["localtime_epoch"],
    }
    return RawTemperatureRecord(**record_dict)


def geo_record_to_raw_temperature_record(
    geo_record: GeoRecord,
) -> Union[RawTemperatureRecord, None]:
    try:
        current_weather_dict = get_current_weather_json(
            geo_record.latitude, geo_record.longitude
        )
        weather_record = current_weather_to_raw_temperature_record(current_weather_dict)
        print(f"got {weather_record}")
        return weather_record
    except:
        log.error(
            "Error retrieving weather data response for coordinates %f, %f - skipping...",
            geo_record.latitude,
            geo_record.longitude,
            exc_info=True,
        )


def process_coordinates():
    print(f"connecting to Kafka at {KAFKA_HOST}")
    consumer = KafkaConsumer(
        TWITTER_GEO_TOPIC, api_version=(0, 10, 1), bootstrap_servers=KAFKA_HOST
    )
    producer = KafkaProducer(api_version=(0, 10, 1), bootstrap_servers=KAFKA_HOST)
    print(f"starting coordinate processing from kafka topic {TWITTER_GEO_TOPIC}...")
    write_path = RAW_TEMPERATURE_FILE_PATH
    write_header = not os.path.isfile(write_path)
    headers = list(RawTemperatureRecord.schema()["properties"].keys())
    with open(write_path, "a") as out_file:
        csv_writer = csv.DictWriter(out_file, fieldnames=headers)
        for message in consumer:
            print(f"received {message.value}")
            geo_record = GeoRecord(**deserialize_record(message.value))
            print(f"getting temperature data for {geo_record}")
            raw_temp_record = geo_record_to_raw_temperature_record(geo_record)
            if raw_temp_record:
                if write_header:
                    csv_writer.writeheader()
                    write_header = False
                print("writing temperature data")
                csv_writer.writerow(raw_temp_record.dict())
                out_file.flush()
                producer.send(RAW_TEMPERATURE_TOPIC, raw_temp_record.serialize())


if __name__ == "__main__":
    print("coordinate_to_temperature_processor.py starting...")
    process_coordinates()
